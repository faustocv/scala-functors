// Exercises

import cats.Functor
import cats.implicits._

// 1: Write a Functor

// trait Functor[F[_]] {
//   def map[A, B](fa: F[A])(f: A => B): F[B]
// }


object Main extends App {
  // 2: Define a Functor instance for List type constructor
  // val listFunctor: Functor[List] = new Functor[List] {
  //   def map[A, B](fa: List[A])(f: A => B) = fa map f
  // }

  // 3: Define a Functor instance for Option type constructor
  // val optionFunctor: Functor[Option] = new Functor[Option] {
  //   def map[A, B](fa: Option[A])(f: A => B): Option[B] = fa match {
  //     case None => None
  //     case Some(a) => Some(f(a))
  //   } 
  // }

  // println(listFunctor.map(List("qwer", "asdfgh"))(_.length))
  // println(optionFunctor.map(Option(1))(_ + 1))
  // println(optionFunctor.map(None)(x => x))

  println(Functor[List].map(List("qwer", "asdfgh"))(_.length))
  println(Functor[Option].map(Option(1))(_ + 1))
  println(Functor[Option].map(None)(x => x))

  // Functors compose
  //4: Map elements of the list adding 1 unit
  val listOption = List(Some(1), None, Some(2))
  println(listOption.map(_.map(_ + 1)))
  println(Functor[List].compose[Option].map(listOption)(_ + 1))
}